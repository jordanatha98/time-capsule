# Time Capsule
This Time Capsule is a business based on people who are handling events with souvenirs.

Made by using Moralis and ReactJS.
This application is connected to Rinkeby smart contract and also Rinkeby Testnet using Moralis.
If there is a problem with the program, please contact: 

Jordanatha - +621380009110, jordanatha98@gmail.com
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Step by Step
Start with `npm install` command in the terminal, if the packages are successfully installed, run `npm start`and open localhost:3000


Start by connecting your Metamask in the home page.

After connected, you will be redirected to the dashboard page for you to input guest lists. Click submit to input guest, and when you're done, click "Generate List" to generate the list into a readable JSON file for the system to read later

Next, you will be redirected to /upload page, in the upload JSON file, upload the saved JSON file from the step before.
this page is for you to upload the file to mint (create the NFT) and also, 
generating the token and sending a message to the customers containing the link to redirect 
them to /receive and the tokenId

To see how the user will receive the NFT, go to localhost:3000/receive, 
and insert the data needed with your wallet address and the tokenId that you saw in /upload.
NOTE that if the NFT in your account is not in the list, you need to import it manually from: https://rinkeby.etherscan.io/address/0xb5de0d36b5e1f549ec6f73d069b04930197cd4cc
and go to your transaction. It is because of the MetaMask issue. 

You can also see the results in opensea using this account:
https://testnets.opensea.io/collection/mynft-kyhchp4mri?search[sortAscending]=false&search[sortBy]=CREATED_DATE



