import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { MoralisProvider } from "react-moralis";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Dashboard from './Dashboard';
import Image from './Image';
import Receive from './Receive';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <React.StrictMode>
            <MoralisProvider serverUrl="https://3prj3pj8exws.usemoralis.com:2053/server" appId="SLK7r4QmKehPs83eaq76GGQ5D5U6sXA89Pq1Lduk">
                <Routes>
                    <Route path="/" element={<App />} />
                    <Route path="dashboard" element={<Dashboard />} />
                    <Route path="upload" element={<Image />} />
                    <Route path="receive" element={<Receive />} />
                </Routes>
            </MoralisProvider>
        </React.StrictMode>
    </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
