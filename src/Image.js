import {useState} from "react";
import {useMoralisFile, useMoralisWeb3Api} from "react-moralis";
import Moralis from "moralis";
import Web3 from "web3";
import logo from "./assets/logo-white.png";
import './Dashboard.css';

const nft_contract_address = "0x0Fb6EF3505b9c52Ed39595433a21aF9B5FCc4431"
const web3 = new Web3(window.ethereum);

async function mintToken(_uri){
    const encodedFunction = web3.eth.abi.encodeFunctionCall({
        name: "mintToken",
        type: "function",
        inputs: [{
            type: 'string',
            name: 'tokenURI'
        }]
    }, [_uri]);

    const transactionParameters = {
        to: nft_contract_address,
        from: window.ethereum.selectedAddress,
        data: encodedFunction
    };
    const txt = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters]
    });
    return txt
}

function Image() {
    const [fileTarget, setFileTarget] = useState("");
    const [jsonTarget, setJsonTarget] = useState("");
    const [nftTarget, setNftTarget] = useState("");
    const { saveFile } = useMoralisFile();
    const Web3Api = useMoralisWeb3Api();

    const fetchTokenMetadata = async (_id) => {
        const testnetNFTs = await Web3Api.Web3API.account.getNFTs({
            chain: "rinkeby",
            address: window.ethereum.selectedAddress
        });
        console.log(testnetNFTs);
        nftInput(testnetNFTs.result[0]);
    };

    const uploadFile = () => {
        saveFile("photo.jpg", fileTarget, {
            type: "image/png",
            saveIPFS: true,
            onSuccess: (result) => {
                mintToken(result.ipfs()).then((res) => {
                    fetchTokenMetadata(res).then();
                })
            },
            onError: (error) => console.log(error),
        });
    };

    const fileInput = (e) => setFileTarget(e.target.files[0]);
    const jsonInput = (e) => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            console.log("e.target.result", e.target.result);
            const parsed = JSON.parse(e.target.result);
            setJsonTarget(parsed[0]);
        };
    }

    const nftInput = (e) => setNftTarget(e.token_id);

    const ShowMessage = () => {
        if (jsonTarget !== '' && nftTarget !== '') {
            return (
                <div className="success-alert">
                    Success minting & sending message to {jsonTarget.name} with token {nftTarget}
                </div>
            )
        }
    }

    return (
        <div>
            <div>
                <div className="dashboard">
                    <div className="center-wrapper">
                        <img alt="logo" src={logo}/>
                    </div>
                    <div className="center-wrapper form-wrap">
                        <div>
                            <div className="form-label">
                                Upload Customer JSON Data
                            </div>
                            <input type="file" onChange={jsonInput} accept=".json"/>
                        </div>
                    </div>
                    <div className="center-wrapper form-wrap">
                        <div>
                            <div className="form-label">
                                Image to mint
                            </div>
                            <input type="file" onChange={fileInput} />
                        </div>
                    </div>
                    <div className="center-wrapper">
                        <div className="center-wrapper">
                            <button onClick={uploadFile} type="submit" className="submit-button-mint">Mint & Send Message</button>
                        </div>
                    </div>
                </div>
                <div className="center-wrapper">
                    <ShowMessage/>
                </div>
            </div>
        </div>
    );
}

export default Image;