import React from 'react';
import './Dashboard.css';
import logo from './assets/logo-white.png';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.handleAdd = this.handleAdd.bind(this);
        this.state = {
            data: []
        }
    }

    onChangeValue = event => {
        this.setState({ value: event.target.value });
    };

    async handleAdd(event) {
        if (event.target[0].value !== '' && event.target[1] !== '') {
            let tempData = { name: event.target[0].value, phone: event.target[1].value };
            await this.setState({ data: [...this.state.data, tempData] }, () => {
                event.target.reset();
            })
        }
        event.preventDefault();
    }

    exportData = () => {
        const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
            JSON.stringify(this.state.data)
        )}`;
        const link = document.createElement("a");
        link.href = jsonString;
        link.download = "data.json";

        link.click();
        window.open('http://localhost:3000/upload', '_self')
    };

    render() {
        return (
            <div>
                <div className="dashboard">
                    <div className="center-wrapper">
                        <img alt="logo" src={logo}/>
                    </div>
                    <form onSubmit={this.handleAdd}>
                        <div className="center-wrapper form-wrap">
                            <div>
                                <div className="form-label">
                                    Name
                                </div>
                                <input type="text" />
                            </div>
                        </div>
                        <div className="center-wrapper form-wrap">
                            <div>
                                <div className="form-label">
                                    Phone Number
                                </div>
                                <input type="text" />
                            </div>
                        </div>
                        <div className="center-wrapper">
                            <input type="submit" value="Submit" className="submit-button" />
                        </div>
                    </form>
                </div>
                <div className="center-wrapper guest-wrapper">
                    <div className="min-600">
                        <div className="center-wrapper">
                            <div className="title">Guest List</div>
                        </div>
                        <ul>
                            {this.state.data.map(item => (
                                <li className="data-box" key={item.name}>
                                    <div className="data-label">{item.name}</div>
                                    <div>{item.phone}</div>
                                </li>
                            ))}
                        </ul>
                        <div className="right-wrapper">
                            <button onClick={this.exportData} className="generate-button">Generate List</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;
