import React, {useState} from "react";
import {useMoralis, useWeb3Transfer} from "react-moralis";
import Moralis from "moralis";
import logo from "./assets/logo-white.png";

const Receive = () => {
    const [addressTarget, setAddressTarget] = useState("");
    const [tokenTarget, setTokenTarget] = useState("");
    const [messageTarget, setMessageTarget] = useState(false);

    const {fetch, error, isFetching} = useWeb3Transfer({
        type: "erc721",
        receiver: addressTarget,
        contractAddress: "0x0fb6ef3505b9c52ed39595433a21af9b5fcc4431",
        tokenId: tokenTarget,
    });

    async function submit(){
        const web3 = await Moralis.enableWeb3();
        try {
            fetch({
                throwOnError: true,
                onSuccess: (e) => {
                    setMessageTarget(true)
                }
            })
        } catch (error) {
            console.log(error);
        }
    }

    const ShowMessage = () => {
        if (messageTarget === true) {
            return (
                <div className="success-alert">
                    Success receiving NFT
                </div>
            )
        }
    }

    const addressInput = (e) => setAddressTarget(e.target.value);
    const tokenInput = (e) => setTokenTarget(e.target.value);
    const messageInput = (e) => setMessageTarget(e);

    return (
        <div>
            <div className="dashboard">
                <div className="center-wrapper">
                    <img alt="logo" src={logo}/>
                </div>
                <div className="center-wrapper form-wrap">
                    <div>
                        <div className="form-label">
                            Wallet Address
                        </div>
                        <input type="text" onChange={addressInput} />
                    </div>
                </div>
                <div className="center-wrapper form-wrap">
                    <div>
                        <div className="form-label">
                            Token Number
                        </div>
                        <input type="text" onChange={tokenInput} />
                    </div>
                </div>
                <div className="center-wrapper">
                    <button onClick={submit} type="submit" className="submit-button">Receive</button>
                </div>
            </div>
            <div className="center-wrapper">
                <ShowMessage/>
            </div>
        </div>
    );
};
export default Receive;