import React, {useEffect} from 'react';
import './App.css';
import metamask from './assets/metamask.png';
import {useMoralis} from "react-moralis";
import {useNavigate} from "react-router-dom";

function App() {
    let navigate = useNavigate();
    const routeChange = () =>{
        let path = `/dashboard`;
        navigate(path);
    }

    const { authenticate, isAuthenticated, isAuthenticating, user, account, logout } = useMoralis();

    useEffect(() => {
        if (isAuthenticated) {
            // add your logic here
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isAuthenticated]);

    const login = async () => {
        console.log(isAuthenticated)
        /*if (!isAuthenticated) {

        }*/
        await authenticate({signingMessage: "Log in using Moralis" })
            .then(function (user) {
                console.log("logged in user:", user);
                console.log(user.get("ethAddress"));
                routeChange();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <div className="App">
            <div className="overlay">
            </div>

            <div className="button-overlay-wrapper">
                <div onClick={login} className="btn-metamask">
                    <img alt="metamask" src={metamask} className="metamask-logo"/>
                    Connect with MetaMask
                </div>
            </div>

            <img alt="background" className="background" src="background.jpg"/>
        </div>
    );
}

export default App;
